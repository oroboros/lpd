LPD: Lichess PGN Downloader.

Простой скрипт, предназначенный для получения PGN-файлов с сайта lichess (http://lichess.org).
Скрипт был написан, так как сам этот шахматный сайт не позволяет загрузить непосредственно PGN-файлы, предоставляя лишь экспорт игр в CSV-файл. Никакой особой  магией скрипт не обладает, он просто вытягивает из CSV-файла ссылки и, используя их, загружает PGN-файлы в текущую директорию.

Имя CSV-файла необходимо передать скрипту первым аргументом.
Для работы необходим Python 2.7.
